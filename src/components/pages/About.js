import React from 'react'

function About() {
  return (
    <React.Fragment>
      <h1>About</h1>
      <p>This is the Todo List web App. </p>

      <h2>Features Includes:</h2>
      <ul>
        <li>React</li>
        <li>Bootstrap for layout</li>
        <li>Axios for api calls</li>
      </ul>
    </React.Fragment>
  )
}


export default About;