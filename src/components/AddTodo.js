import React, { Component } from 'react';
import PropTypes from 'prop-types';

export class AddTodo extends Component {
  state = {
    title: ''
  }

  onSubmit = (e) => {
    e.preventDefault();
    this.props.addTodo(this.state.title);
    this.setState({ title: '' });
  }

  onChange = (e) => this.setState({ [e.target.name]: e.target.value });

  render() {
    return (
      <form onSubmit={this.onSubmit} style={{ display: 'flex', margin: '30px 0px' }}>
        <input 
          type="text" 
          name="title" 
          style={{ flex: '10', padding: '5px 15px',height: '50px' }}
          placeholder="Add Todo ..." 
          value={this.state.title}
          onChange={this.onChange}
          className="form-control"
        />
        <input 
          type="submit" 
          value="Submit" 
          className="btn btn-primary"
          style={{flex: '1'}}
        />
      </form>
    )
  }
}

// PropTypes
AddTodo.propTypes = {
  addTodo: PropTypes.func.isRequired
}

export default AddTodo
